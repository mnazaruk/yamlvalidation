package trying;

import trying.annotations.UniqueId;

public class Embedded {

    @UniqueId
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
