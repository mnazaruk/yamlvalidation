package trying.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import trying.Model;
import trying.annotations.UniqueId;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class UniqueIdValidator implements ConstraintValidator<UniqueId, String> {
    private Set<String> ids = new HashSet<>();

    private static final Logger LOGGER = LoggerFactory.getLogger(UniqueIdValidator.class);

    @Override
    public void initialize(UniqueId uniqueId) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext context) {
        if (!ids.contains(s)) {
            ids.add(s);
            return true;
        }
        else {
            LOGGER.info("contains " + s);
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(
                    s + " is already exists ")
                    .addConstraintViolation();
            return false;
        }
    }
}
