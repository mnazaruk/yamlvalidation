package trying.validators;

import trying.annotations.EmailExistsConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;

public class EmailExistsConstraintValidator implements ConstraintValidator<EmailExistsConstraint, String>{

    List<String> availableEmails = Arrays.asList("1@a.b", "2@a.b", "3@a.b");

    @Override
    public void initialize(EmailExistsConstraint annotation) {

    }

    @Override
    public boolean isValid(String o, ConstraintValidatorContext constraintValidatorContext) {

        return availableEmails.contains(o);
    }
}
