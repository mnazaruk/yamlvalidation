package trying.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import trying.annotations.FromEnum;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

public class EnumValidator implements ConstraintValidator<FromEnum, String> {

    private Set<String> availableStrings;

    private static final Logger LOGGER = LoggerFactory.getLogger(UniqueIdValidator.class);

    @Override
    public void initialize(FromEnum fromEnum) {
        availableStrings = new HashSet<>();
        Class<? extends Enum> source = fromEnum.source();
        Enum[] enumConstants = source.getEnumConstants();

        for (Enum enumConstant : enumConstants) {
            availableStrings.add(enumConstant.name().toLowerCase());
            LOGGER.info("adding " + enumConstant.name());
        }
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext context) {
        if (availableStrings.contains(s.toLowerCase())) {
            return true;
        }
        else {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(
                    s + " is not available value ")
                    .addConstraintViolation();
            return false;
        }
    }
}
