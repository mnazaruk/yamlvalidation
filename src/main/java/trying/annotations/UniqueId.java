package trying.annotations;

import trying.validators.UniqueIdValidator;

import javax.validation.Constraint;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = UniqueIdValidator.class)

public @interface UniqueId {

    String message() default "Id is not unique";
    Class[] groups() default {};
    Class[] payload() default {};
}
