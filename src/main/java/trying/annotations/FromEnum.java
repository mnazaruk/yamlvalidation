package trying.annotations;

import com.strevus.app.common.core.services.config.yaml.enums.FacetTypable;
import trying.Embedded;
import trying.validators.EnumValidator;

import javax.validation.Constraint;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = EnumValidator.class)

public @interface FromEnum {

    String message() default "Email doesn't exists";
    Class[] groups() default {};
    Class[] payload() default {};
    Class<? extends Enum> source();
}
