package trying;

import com.strevus.app.common.core.services.config.yaml.enums.FacetType;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import trying.annotations.EmailExistsConstraint;
import trying.annotations.FromEnum;
import trying.annotations.Lalala;
import trying.annotations.UniqueId;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Model implements Serializable{

    @NotNull
    @FromEnum(source = FacetType.class)
    String value;

    @Valid
    List<Embedded> list = new ArrayList<>();

    @Valid
    List<Embedded> list1 = new ArrayList<>();

    @NotNull
    @Length(min=1)
    @Email
    @EmailExistsConstraint
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Embedded> getList() {
        return list;
    }

    public void setList(List<Embedded> list) {
        this.list = list;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
