/*******************************************************************************
 Copyright (c) 2014. Strevus, Inc.  All rights reserved
 ******************************************************************************/

package com.strevus.app.common.core.services.config.yaml.enums;

/**
 * Enumeration of configs that can be initialized dynamically.
 */
public enum FacetConfigType implements FacetTypable{

    // used at FATCA campaign creation phase
    FATCA_CAMPAIGN("FATCA_CAMPAIGN"),

    // used at FATCA campaign overview phase
    FATCA_DRILLDOWN("FATCA_DRILLDOWN"),

    // used at KYC campaign creation phase
    KYC_CAMPAIGN("KYC_CAMPAIGN"),

    // used at KYC campaign overview phase
    KYC_DRILLDOWN("KYC_DRILLDOWN"),

    // used at overview phase for campaigns that support both KYC and FATCA
    DEFAULT_DRILLDOWN("DEFAULT_DRILLDOWN");

    private FacetConfigType(String configName) {
        this.configName = configName.toLowerCase();
    }

    private String configName;

    FacetConfigType() {

    }

    /**
     * @param configName
     * @return config type or {@code null} if no type defined for specified configName
     */
    public static FacetConfigType getByName(String configName) {
        for (FacetConfigType configType: values()) {
            if (configType.getName().toLowerCase().equals(configName.toLowerCase())) {
                return configType;
            }
        }

        return null;
    }


    public String getName() {
        return configName;
    }
}
