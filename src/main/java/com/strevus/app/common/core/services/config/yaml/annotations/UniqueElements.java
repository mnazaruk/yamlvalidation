package com.strevus.app.common.core.services.config.yaml.annotations;

import com.strevus.app.common.core.services.config.yaml.validators.UniqueValidator;

import javax.validation.Constraint;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = UniqueValidator.class)

public @interface UniqueElements {

    String message() default "Element is not unique";
    Class[] groups() default {};
    Class[] payload() default {};
}
