package com.strevus.app.common.core.services.config.yaml.annotations;

import com.strevus.app.common.core.services.config.yaml.TestWithFields;
import com.strevus.app.common.core.services.config.yaml.validators.FiledExistsValidator;
import trying.validators.UniqueIdValidator;

import javax.validation.Constraint;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = FiledExistsValidator.class)

public @interface FieldExistsInClass {
    Class<?> source();

    String message() default "Not valid config name";
    Class[] groups() default {};
    Class[] payload() default {};
}
