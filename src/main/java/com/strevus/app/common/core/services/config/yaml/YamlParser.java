/*******************************************************************************
 Copyright (c) 2014. Strevus, Inc.  All rights reserved
 ******************************************************************************/
package com.strevus.app.common.core.services.config.yaml;

import com.strevus.app.common.core.services.config.yaml.exception.InvalidYamlSyntaxException;
import com.strevus.app.common.core.services.config.yaml.exception.YamlFileNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class YamlParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(YamlParser.class);

    private YamlParser() {}

    public static <T> T load(String fileName) {
        T contentBean = null;
        try (InputStream input = YamlParser.class.getResourceAsStream("/" + fileName)) {
            if (input == null) {
                throw new FileNotFoundException();
            }
            Yaml yaml = new Yaml();
            contentBean = (T) yaml.load(input);
        } catch (FileNotFoundException e) {
            throw new YamlFileNotFoundException("Yml file '" + fileName + "' is not found!", e);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        } catch (Exception e) {
            throw new InvalidYamlSyntaxException(e.getMessage(), e);
        }

        return contentBean;
    }
}
