package com.strevus.app.common.core.services.config.yaml.validators;

import com.strevus.app.common.core.services.config.yaml.TestWithFields;
import com.strevus.app.common.core.services.config.yaml.annotations.FieldExistsInClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import trying.validators.UniqueIdValidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;

public class FiledExistsValidator implements ConstraintValidator<FieldExistsInClass, String> {

    private Set<String> availableStrings = new HashSet<>();

    private static final Logger LOGGER = LoggerFactory.getLogger(UniqueIdValidator.class);

    @Override
    public void initialize(FieldExistsInClass fieldExistsInClass) {

        Class<?> source = fieldExistsInClass.source();
        for (Field field : source.getDeclaredFields()) {
            LOGGER.info("field added " + field.getName());
            availableStrings.add(field.getName());
        }
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext context) {
        if (availableStrings.contains(s)) {
            return true;
        }
        else {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(
                    s + " is not available value for field name.")
                    .addConstraintViolation();
            return false;
        }
    }
}
