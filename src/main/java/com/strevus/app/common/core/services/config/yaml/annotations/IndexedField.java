package com.strevus.app.common.core.services.config.yaml.annotations;

import com.strevus.app.common.core.services.config.yaml.validators.FiledExistsValidator;
import com.strevus.app.common.core.services.config.yaml.validators.IndexedFieldValidator;

import javax.validation.Constraint;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = IndexedFieldValidator.class)

public @interface IndexedField {
    String message() default "Is not indexed field";
    Class[] groups() default {};
    Class[] payload() default {};
}
