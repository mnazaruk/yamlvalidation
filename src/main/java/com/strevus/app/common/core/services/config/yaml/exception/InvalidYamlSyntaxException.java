/*******************************************************************************
 Copyright (c) 2014. Strevus, Inc.  All rights reserved
 ******************************************************************************/
package com.strevus.app.common.core.services.config.yaml.exception;

public class InvalidYamlSyntaxException extends RuntimeException {

    public InvalidYamlSyntaxException(String message) {
        super(message);
    }

    public InvalidYamlSyntaxException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
