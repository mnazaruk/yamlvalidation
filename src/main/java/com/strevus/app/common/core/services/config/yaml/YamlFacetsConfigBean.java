/*******************************************************************************
 Copyright (c) 2014. Strevus, Inc.  All rights reserved
 ******************************************************************************/
package com.strevus.app.common.core.services.config.yaml;

import com.strevus.app.common.core.services.config.yaml.annotations.ValueFromEnum;
import com.strevus.app.common.core.services.config.yaml.enums.FacetConfigType;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

public class YamlFacetsConfigBean {

    @NotBlank
    @ValueFromEnum(source = FacetConfigType.class)
    private String configName;

    @NotEmpty
    private List<String> facets;

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public List<String> getFacets() {
        return facets;
    }

    public void setFacets(List<String> facets) {
        this.facets = facets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof YamlFacetsConfigBean)) return false;

        YamlFacetsConfigBean that = (YamlFacetsConfigBean) o;

        if (configName != null ? !configName.equals(that.configName) : that.configName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return configName != null ? configName.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "YamlFacetsConfigBean{" +
                "configName='" + configName + '\'' +
                '}';
    }
}
