package com.strevus.app.common.core.services.config.yaml.annotations;

import com.strevus.app.common.core.services.config.yaml.enums.FacetTypable;
import com.strevus.app.common.core.services.config.yaml.validators.ValueFromEnumValidator;

import javax.validation.Constraint;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({java.lang.annotation.ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = ValueFromEnumValidator.class)

public @interface ValueFromEnum {

    String message() default "Not valid facet name";
    Class[] groups() default {};
    Class[] payload() default {};

    Class<? extends FacetTypable> source();
}
