/*******************************************************************************
 Copyright (c) 2014. Strevus, Inc.  All rights reserved
 ******************************************************************************/
package com.strevus.app.common.core.services.config.yaml.enums;

/**
 * Enumeration of existing facets that can be used for dynamic configuration.
 */
public enum FacetType implements FacetTypable{

    BUSINESS_LINE("BUSINESS_LINE"),

    COUNTRIES("COUNTRIES"),

    FATCA_CLASS("FATCA_CLASS"),

    IGA_SCENARIO("IGA_SCENARIO"),

    RISK_LEVEL("RISK_LEVEL");

    private FacetType(String name) {
        this.name = name.toLowerCase();
    }

    private String name;

    FacetType() {

    }

    /**
     * @param name
     * @return facet type or {@code null} if no type defined for specified name
     */
    public static FacetType getByName(String name) {
        for (FacetType type: values()) {
            if (type.getName().toLowerCase().equals(name.toLowerCase())) {
                return type;
            }
        }

        return null;
    }


    public String getName() {
        return name;
    }
}
