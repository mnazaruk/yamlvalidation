package com.strevus.app.common.core.services.config.yaml.validators;

import com.strevus.app.common.core.services.config.yaml.annotations.IndexedField;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IndexedFieldValidator implements ConstraintValidator<IndexedField, String>{
    @Override
    public void initialize(IndexedField indexedField) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext context) {
        return true;
    }
}
