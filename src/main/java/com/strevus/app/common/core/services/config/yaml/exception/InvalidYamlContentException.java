/*******************************************************************************
 Copyright (c) 2014. Strevus, Inc.  All rights reserved
 ******************************************************************************/
package com.strevus.app.common.core.services.config.yaml.exception;

public class InvalidYamlContentException extends RuntimeException {

    public InvalidYamlContentException(String message) {
        super(message);
    }

    public InvalidYamlContentException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
