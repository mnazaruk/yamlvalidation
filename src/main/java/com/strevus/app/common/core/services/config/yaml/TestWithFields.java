package com.strevus.app.common.core.services.config.yaml;

import com.strevus.app.common.core.services.config.yaml.enums.FacetTypable;
import com.strevus.app.common.core.services.config.yaml.enums.FacetType;

public class TestWithFields {
    private String field1;
    private Integer field2;
    private Double field3;

    public String getField1() {
        return field1;
    }


    public void setField1(String field1) {
        this.field1 = field1;
    }

    public Integer getField2() {
        return field2;
    }

    public void setField2(Integer field2) {
        this.field2 = field2;
    }

    public Double getField3() {
        return field3;
    }

    public void setField3(Double field3) {
        this.field3 = field3;
    }
}
