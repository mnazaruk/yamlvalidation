/*******************************************************************************
 Copyright (c) 2014. Strevus, Inc.  All rights reserved
 ******************************************************************************/
package com.strevus.app.common.core.services.config.yaml;

import com.strevus.app.common.core.services.config.yaml.annotations.IndexedField;
import com.strevus.app.common.core.services.config.yaml.annotations.ValueFromEnum;
import com.strevus.app.common.core.services.config.yaml.enums.FacetType;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

public class YamlNewFacetBean {

    @ValueFromEnum(source = FacetType.class)
    private String facetName;

    @NotBlank
    @IndexedField
    private String indexFieldName;

    @NotEmpty
    private List<String> facetValues;

    public String getFacetName() {
        return facetName;
    }

    public void setFacetName(String facetName) {
            this.facetName = facetName;
        }

    public String getIndexFieldName() {
            return indexFieldName;
        }

    public void setIndexFieldName(String indexFieldName) {
            this.indexFieldName = indexFieldName;
        }

    public List<String> getFacetValues() {
            return facetValues;
        }

    public void setFacetValues(List<String> facetValues) {
            this.facetValues = facetValues;
        }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof YamlNewFacetBean)) return false;

        YamlNewFacetBean that = (YamlNewFacetBean) o;

        if (facetName != null ? !facetName.equals(that.facetName) : that.facetName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return facetName != null ? facetName.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "YamlNewFacetBean{" +
                "facetName='" + facetName + '\'' +
                '}';
    }
}