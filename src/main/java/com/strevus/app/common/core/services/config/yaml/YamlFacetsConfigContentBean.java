/*******************************************************************************
 Copyright (c) 2014. Strevus, Inc.  All rights reserved
 ******************************************************************************/
package com.strevus.app.common.core.services.config.yaml;

import com.strevus.app.common.core.services.config.yaml.annotations.UniqueElements;

import javax.validation.Valid;
import java.util.List;

public class YamlFacetsConfigContentBean {

    @Valid
    @UniqueElements
    private List<YamlNewFacetBean> newFacets;

    @Valid
    @UniqueElements
    private List<YamlFacetsConfigBean> facetConfigs;

    public List<YamlFacetsConfigBean> getFacetConfigs() {
        return facetConfigs;
    }

    public void setFacetConfigs(List<YamlFacetsConfigBean> facetConfigs) {
        this.facetConfigs = facetConfigs;
    }

    public List<YamlNewFacetBean> getNewFacets() {
        return newFacets;
    }

    public void setNewFacets(List<YamlNewFacetBean> newFacets) {
        this.newFacets = newFacets;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(this.getClass().getSimpleName());

        if (newFacets != null && newFacets.size() > 0) {
            builder.append(System.lineSeparator());
            builder.append("newFacets:");
            for (YamlNewFacetBean bean: newFacets) {
                builder.append(System.lineSeparator());
                builder.append("\t");
                builder.append("facetName: ");
                builder.append(bean.getFacetName());
                builder.append(System.lineSeparator());
                builder.append("\t");
                builder.append("facetValues: ");
                builder.append(bean.getFacetValues());
                builder.append(System.lineSeparator());
                builder.append("\t");
                builder.append("indexFieldName: ");
                builder.append(bean.getIndexFieldName());
            }
        }

        if (facetConfigs != null && facetConfigs.size() > 0) {
            builder.append(System.lineSeparator());
            builder.append("facetConfigs:");
            for (YamlFacetsConfigBean bean: facetConfigs) {
                builder.append(System.lineSeparator());
                builder.append("\t");
                builder.append("configName: ");
                builder.append(bean.getConfigName());
                builder.append(System.lineSeparator());
                builder.append("\t");
                builder.append("facets: ");
                builder.append(bean.getFacets());
            }
        }

        return builder.toString();
    }
}
