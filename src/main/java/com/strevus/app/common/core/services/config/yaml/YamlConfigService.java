/*******************************************************************************
 Copyright (c) 2014. Strevus, Inc.  All rights reserved
 ******************************************************************************/
package com.strevus.app.common.core.services.config.yaml;

import com.strevus.app.common.core.services.config.yaml.exception.InvalidYamlContentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class YamlConfigService {

    private static final Logger LOGGER = LoggerFactory.getLogger(YamlConfigService.class);

    private static final String YAML_FACETS_CONFIG_FILE = "yaml/facets.yaml";

    public YamlFacetsConfigContentBean getValidatedYamlFacetsConfiguration() {
        YamlFacetsConfigContentBean contentBean = YamlParser.load(YAML_FACETS_CONFIG_FILE);
        LOGGER.info("Config file {} is loaded and parsed", YAML_FACETS_CONFIG_FILE);

        validateFacetsConfigContent(contentBean);

        return contentBean;
    }

    private void validateFacetsConfigContent(YamlFacetsConfigContentBean contentBean) {

        if (false) {
            throw new InvalidYamlContentException(null, null);
        }
    }
}
