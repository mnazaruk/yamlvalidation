package com.strevus.app.common.core.services.config.yaml.validators;

import com.strevus.app.common.core.services.config.yaml.annotations.UniqueElements;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class UniqueValidator implements ConstraintValidator<UniqueElements, List> {

    @Override
    public void initialize(UniqueElements uniqueElements) {
    }

    @Override
    public boolean isValid(List list, ConstraintValidatorContext context) {
        Set elements = new HashSet<>();

        if (list == null) {
            return true;
        }
        for (Object o : list) {
            if (!elements.contains(o)) {
                elements.add(o);
            } else {context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(
                        o + " is already exists in list.")
                        .addConstraintViolation();
                return false;
            }
        }
        return true;
    }
}
