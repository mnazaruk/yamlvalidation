/*******************************************************************************
 Copyright (c) 2014. Strevus, Inc.  All rights reserved
 ******************************************************************************/
package com.strevus.app.common.core.services.config.yaml.exception;

public class YamlFileNotFoundException extends RuntimeException {

    public YamlFileNotFoundException(String message) {
        super(message);
    }

    public YamlFileNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
