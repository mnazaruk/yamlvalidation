package com.strevus.app.common.core.services.config.yaml.validators;

import com.strevus.app.common.core.services.config.yaml.annotations.ValueFromEnum;
import com.strevus.app.common.core.services.config.yaml.enums.FacetTypable;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

public class ValueFromEnumValidator implements ConstraintValidator<ValueFromEnum, String> {

    private Set<String> availableValues = new HashSet<>();
    
    @Override
    public void initialize(ValueFromEnum valueFromEnum) {
        if (!valueFromEnum.source().isEnum()) {
            throw new RuntimeException("not a enum");
        } else {
            for (FacetTypable facetTypable : valueFromEnum.source().getEnumConstants()) {
                availableValues.add(facetTypable.getName());
            }
        }
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext context) {
        if (availableValues.contains(s)) {
            return true;
        }
        else {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(
                    s + " is not available value for this field.")
                    .addConstraintViolation();
            return false;
        }
    }
}
