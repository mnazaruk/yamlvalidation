package trying;

import junit.framework.TestCase;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Arrays;
import java.util.Set;

public class IdValidatorTest extends TestCase {

    private static final Logger LOGGER = LoggerFactory.getLogger(IdValidatorTest.class);
    @Override
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void testValidatorPositive() throws Exception {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Model model = new Model();
        Embedded e1 = new Embedded();
        e1.setId("123");
        model.setValue("element1");
//        Embedded e2 = new Embedded();
//        e2.setId("321");

        model.setList(Arrays.asList(e1));
        model.setEmail("1@a.b");

        Set<ConstraintViolation<Model>> violations = validator.validate(model);
        assertEquals(0, violations.size());

    }

    @Test
    public void testValidatorNegative() throws Exception {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        String messageExpected = "123 is already exists ";
        String message = "";
        Model model = new Model();
        model.setValue("element1");
        Embedded e1 = new Embedded();
        e1.setId("123");
        Embedded e2 = new Embedded();
        e2.setId("123");

        Embedded e3 = new Embedded();
        e3.setId("321");

        model.setList(Arrays.asList(e1, e2));
        model.setEmail("1@a.b");

        Set<ConstraintViolation<Model>> violations = validator.validate(model);
        validator.validate(model);
        assertEquals(1, violations.size());
        for (ConstraintViolation<Model> violation : violations) {
            message = violation.getMessage();
        }
        assertEquals(messageExpected, message);
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }
}