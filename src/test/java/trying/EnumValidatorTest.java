package trying;

import com.strevus.app.common.core.services.config.yaml.enums.FacetTypable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class EnumValidatorTest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void validatorTestPositive() {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Model model = new Model();
        model.setEmail("1@a.b");
        model.setValue("element1");

        Set<ConstraintViolation<Model>> violations = validator.validate(model);

        assertEquals(0, violations.size());

        model.setValue("element2");

        violations = validator.validate(model);

        assertEquals(0, violations.size());

        model.setValue("element3");

        violations = validator.validate(model);

        assertEquals(0, violations.size());
    }


    @Test
    public void validatorTestNegative() {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Model model = new Model();
        model.setEmail("1@a.b");
        model.setValue("element not from enum");

        String messageExpected = "element not from enum is not available value ";
        String message = "";

        Set<ConstraintViolation<Model>> violations = validator.validate(model);

        assertEquals(1, violations.size());
        for (ConstraintViolation<Model> violation : violations) {
            message = violation.getMessage();
        }
        assertEquals(messageExpected, message);
    }

    @After
    public void tearDown() throws Exception  {

    }

    public class Elala<T extends Enum & FacetTypable> {

    }
}