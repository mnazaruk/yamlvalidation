package trying;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class EmailValidatorTest {

    private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testValidatorPositive() throws Exception {
        Model model = new Model();
        model.setEmail("1@a.b");
        model.setValue("element1");

        Set<ConstraintViolation<Model>> violations = validator.validate(model);

        assertEquals(0, violations.size());
    }

    @Test
    public void testValidatorNegative() throws Exception {
        Model model = new Model();
        model.setEmail("123@a.b");
        model.setValue("element1");
        String messageExpected = "Email doesn't exists";
        String message = "";

        Set<ConstraintViolation<Model>> violations = validator.validate(model);

        assertEquals(1, violations.size());
        for (ConstraintViolation<Model> violation : violations) {
            message = violation.getMessage();
        }
        assertEquals(messageExpected, message);
    }

    @After
    public void tearDown() throws Exception {

    }
}