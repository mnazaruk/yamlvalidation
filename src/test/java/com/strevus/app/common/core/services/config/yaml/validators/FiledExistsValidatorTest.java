package com.strevus.app.common.core.services.config.yaml.validators;

import com.strevus.app.common.core.services.config.yaml.YamlNewFacetBean;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Arrays;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class FiledExistsValidatorTest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testValidatorPositive() throws Exception {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        YamlNewFacetBean yamlNewFacetBean = new YamlNewFacetBean();
        yamlNewFacetBean.setFacetName("risk_level");
        yamlNewFacetBean.setFacetValues(Arrays.asList("el1", "el2"));
        yamlNewFacetBean.setIndexFieldName("field1");

        Set<ConstraintViolation<YamlNewFacetBean>> violations = validator.validate(yamlNewFacetBean);

        assertEquals(0, violations.size());

        yamlNewFacetBean.setIndexFieldName("field2");

        violations = validator.validate(yamlNewFacetBean);

        assertEquals(0, violations.size());

        yamlNewFacetBean.setIndexFieldName("field3");

        violations = validator.validate(yamlNewFacetBean);

        assertEquals(0, violations.size());
    }

    @Ignore
    @Test
    public void testValidatorNegative() throws Exception {

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        YamlNewFacetBean yamlNewFacetBean = new YamlNewFacetBean();
        yamlNewFacetBean.setFacetName("risk_level");
        yamlNewFacetBean.setFacetValues(Arrays.asList("el1", "el2"));

        yamlNewFacetBean.setIndexFieldName("field21234");

        String messageExpected = "field21234 is not available value for field name.";
        String message = "";
        Set<ConstraintViolation<YamlNewFacetBean>>  violations = validator.validate(yamlNewFacetBean);

        assertEquals(1, violations.size());

        for (ConstraintViolation<YamlNewFacetBean> violation : violations) {
            message = violation.getMessage();
        }
        assertEquals(messageExpected, message);
    }

    @After
    public void tearDown() throws Exception {

    }
}