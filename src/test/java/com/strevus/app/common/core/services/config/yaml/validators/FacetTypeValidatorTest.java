package com.strevus.app.common.core.services.config.yaml.validators;

import com.strevus.app.common.core.services.config.yaml.YamlNewFacetBean;
import com.strevus.app.common.core.services.config.yaml.enums.FacetType;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Arrays;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class FacetTypeValidatorTest {

    @Test
    public void testValidatorPositive() throws Exception {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        YamlNewFacetBean yamlNewFacetBean = new YamlNewFacetBean();
        yamlNewFacetBean.setFacetName("risk_level");
        yamlNewFacetBean.setFacetValues(Arrays.asList("el1", "el2"));
        yamlNewFacetBean.setIndexFieldName("field1");

        Set<ConstraintViolation<YamlNewFacetBean>> violations = validator.validate(yamlNewFacetBean);

        assertEquals(0, violations.size());

        yamlNewFacetBean.setFacetName("countries");

        violations = validator.validate(yamlNewFacetBean);

        assertEquals(0, violations.size());
    }

    @Test
    public void testValidatorNegative() throws Exception {

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        YamlNewFacetBean yamlNewFacetBean = new YamlNewFacetBean();
        yamlNewFacetBean.setFacetName("facetName");
        yamlNewFacetBean.setFacetValues(Arrays.asList("el1", "el2"));

        yamlNewFacetBean.setIndexFieldName("field2");

        String messageExpected = "facetName is not available value for this field.";
        String message = "";
        Set<ConstraintViolation<YamlNewFacetBean>>  violations = validator.validate(yamlNewFacetBean);

        assertEquals(1, violations.size());

        for (ConstraintViolation<YamlNewFacetBean> violation : violations) {
            message = violation.getMessage();
        }
        assertEquals(messageExpected, message);
    }

}