package com.strevus.app.common.core.services.config.yaml.validators;

import com.strevus.app.common.core.services.config.yaml.YamlFacetsConfigBean;
import com.strevus.app.common.core.services.config.yaml.YamlFacetsConfigContentBean;
import com.strevus.app.common.core.services.config.yaml.YamlNewFacetBean;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Arrays;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class UniqueFacetNameValidatorTest {

    @Before
    public void setUp() throws Exception {

    }
    @Test
    public void testValidatorPositive() throws Exception {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        YamlNewFacetBean yamlNewFacetBean1 = new YamlNewFacetBean();
        yamlNewFacetBean1.setFacetName("countries");
        yamlNewFacetBean1.setFacetValues(Arrays.asList("el1", "el2"));
        yamlNewFacetBean1.setIndexFieldName("field1");


        YamlNewFacetBean yamlNewFacetBean2 = new YamlNewFacetBean();
        yamlNewFacetBean2.setFacetName("risk_level");
        yamlNewFacetBean2.setFacetValues(Arrays.asList("el1", "el2"));
        yamlNewFacetBean2.setIndexFieldName("field1");


        YamlNewFacetBean yamlNewFacetBean3 = new YamlNewFacetBean();
        yamlNewFacetBean3.setFacetName("fatca_class");
        yamlNewFacetBean3.setFacetValues(Arrays.asList("el1", "el2"));
        yamlNewFacetBean3.setIndexFieldName("field1");

        YamlFacetsConfigBean yamlFacetsConfigBean1 = new YamlFacetsConfigBean();
        yamlFacetsConfigBean1.setConfigName("fatca_drilldown");
        yamlFacetsConfigBean1.setFacets(Arrays.asList("lalal", "countries"));


        YamlFacetsConfigBean yamlFacetsConfigBean2 = new YamlFacetsConfigBean();
        yamlFacetsConfigBean2.setConfigName("kyc_drilldown");
        yamlFacetsConfigBean2.setFacets(Arrays.asList("lalal", "countries"));

        YamlFacetsConfigContentBean configContentBean = new YamlFacetsConfigContentBean();
        configContentBean.setNewFacets(Arrays.asList(yamlNewFacetBean1, yamlNewFacetBean2, yamlNewFacetBean3));
        configContentBean.setFacetConfigs(Arrays.asList(yamlFacetsConfigBean1, yamlFacetsConfigBean2));

        Set<ConstraintViolation<YamlFacetsConfigContentBean>>  violations = validator.validate(configContentBean);

        assertEquals(0, violations.size());
    }

    @Test
    public void testValidatorNegative() throws Exception {

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        YamlNewFacetBean yamlNewFacetBean1 = new YamlNewFacetBean();
        yamlNewFacetBean1.setFacetName("countries");
        yamlNewFacetBean1.setFacetValues(Arrays.asList("el1", "el2"));
        yamlNewFacetBean1.setIndexFieldName("field1");


        YamlNewFacetBean yamlNewFacetBean2 = new YamlNewFacetBean();
        yamlNewFacetBean2.setFacetName("fatca_class");
        yamlNewFacetBean2.setFacetValues(Arrays.asList("el1", "el2"));
        yamlNewFacetBean2.setIndexFieldName("field1");


        YamlNewFacetBean yamlNewFacetBean3 = new YamlNewFacetBean();
        yamlNewFacetBean3.setFacetName("fatca_class");
        yamlNewFacetBean3.setFacetValues(Arrays.asList("el1", "el2"));
        yamlNewFacetBean3.setIndexFieldName("field1");

        YamlFacetsConfigBean yamlFacetsConfigBean1 = new YamlFacetsConfigBean();
        yamlFacetsConfigBean1.setConfigName("fatca_drilldown");
        yamlFacetsConfigBean1.setFacets(Arrays.asList("lalal", "countries"));


        YamlFacetsConfigBean yamlFacetsConfigBean2 = new YamlFacetsConfigBean();
        yamlFacetsConfigBean2.setConfigName("kyc_drilldown");
        yamlFacetsConfigBean2.setFacets(Arrays.asList("lalal", "countries"));

        YamlFacetsConfigContentBean configContentBean = new YamlFacetsConfigContentBean();
        configContentBean.setNewFacets(Arrays.asList(yamlNewFacetBean1, yamlNewFacetBean2, yamlNewFacetBean3));
        configContentBean.setFacetConfigs(Arrays.asList(yamlFacetsConfigBean1, yamlFacetsConfigBean2));


        String messageExpected = "YamlNewFacetBean{facetName='fatca_class'} is already exists in list.";
        String message = "";
        Set<ConstraintViolation<YamlFacetsConfigContentBean>>  violations = validator.validate(configContentBean);

        assertEquals(1, violations.size());

        for (ConstraintViolation<YamlFacetsConfigContentBean> violation : violations) {
            message = violation.getMessage();
        }
        assertEquals(messageExpected, message);
    }

    @After
    public void tearDown() throws Exception {

    }
}