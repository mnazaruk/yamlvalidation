package com.strevus.app.common.core.services.config.yaml.validators;

import com.strevus.app.common.core.services.config.yaml.YamlFacetsConfigBean;
import com.strevus.app.common.core.services.config.yaml.YamlFacetsConfigContentBean;
import com.strevus.app.common.core.services.config.yaml.YamlNewFacetBean;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Arrays;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class EmptyFacetsListTest {

    @Test
    public void testValidatorPositive() throws Exception {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        YamlFacetsConfigBean yamlFacetsConfigBean1 = new YamlFacetsConfigBean();
        yamlFacetsConfigBean1.setConfigName("fatca_drilldown");
        yamlFacetsConfigBean1.setFacets(Arrays.asList("lalal", "countries"));


        YamlFacetsConfigBean yamlFacetsConfigBean2 = new YamlFacetsConfigBean();
        yamlFacetsConfigBean2.setConfigName("kyc_drilldown");
        yamlFacetsConfigBean2.setFacets(Arrays.asList("lalal", "countries"));


        YamlFacetsConfigBean yamlFacetsConfigBean3 = new YamlFacetsConfigBean();
        yamlFacetsConfigBean3.setConfigName("kyc_campaign");
        yamlFacetsConfigBean3.setFacets(Arrays.asList("lalal", "countries"));

        YamlFacetsConfigContentBean configContentBean = new YamlFacetsConfigContentBean();
        configContentBean.setFacetConfigs(Arrays.asList(yamlFacetsConfigBean1, yamlFacetsConfigBean2, yamlFacetsConfigBean3));

        Set<ConstraintViolation<YamlFacetsConfigContentBean>> violations = validator.validate(configContentBean);

    }

}