package com.strevus.app.common.core.services.config.yaml.validators;


import com.strevus.app.common.core.services.config.yaml.YamlFacetsConfigBean;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Arrays;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class FacetConfigTypeValidatorTest {
    @Test
    public void testValidatorPositive() throws Exception {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        YamlFacetsConfigBean yamlFacetsConfigBean = new YamlFacetsConfigBean();
        yamlFacetsConfigBean.setConfigName("fatca_drilldown");
        yamlFacetsConfigBean.setFacets(Arrays.asList("lalal", "countries"));

        Set<ConstraintViolation<YamlFacetsConfigBean>> violations = validator.validate(yamlFacetsConfigBean);

        assertEquals(0, violations.size());

        yamlFacetsConfigBean.setConfigName("kyc_campaign");

        violations = validator.validate(yamlFacetsConfigBean);

        assertEquals(0, violations.size());
    }

    @Test
    public void testValidatorNegative() throws Exception {

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        YamlFacetsConfigBean yamlNewFacetBean = new YamlFacetsConfigBean();
        yamlNewFacetBean.setConfigName("risk_level");
        yamlNewFacetBean.setFacets(Arrays.asList("risk_level", "countries"));


        String messageExpected = "risk_level is not available value for this field.";
        String message = "";
        Set<ConstraintViolation<YamlFacetsConfigBean>>  violations = validator.validate(yamlNewFacetBean);

        assertEquals(1, violations.size());

        for (ConstraintViolation<YamlFacetsConfigBean> violation : violations) {
            message = violation.getMessage();
        }
        assertEquals(messageExpected, message);
    }
}